/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.natasha;

/**
 *
 * @author novebeta
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import jpos.JposException;
import jpos.LineDisplay;

import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketImpl;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshake;

import org.java_websocket.server.WebSocketServer;

/**
 * A simple WebSocketServer implementation. Keeps track of a "chatroom".
 */
public class posiflexServer extends WebSocketServer {

    public LineDisplay ld;

    public final void initPosiflex() {
        try {
            ld = new LineDisplay();
            ld.open("natashaLineDisplay");
            ld.claim(1000);
            ld.setDeviceEnabled(true);
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        ld.release();
                        ld.close();
                    } catch (JposException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            });
        } catch (JposException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    public posiflexServer(int port) throws UnknownHostException {
        super(new InetSocketAddress(port));
        initPosiflex();
    }

    public posiflexServer(InetSocketAddress address) {
        super(address);
        initPosiflex();
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
//        this.sendToAll("new connection: " + handshake.getResourceDescriptor());
        System.out.println(conn.getRemoteSocketAddress().getHostString()+ " entered the room!");
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
//        this.sendToAll(conn + " has left the room!");
        System.out.println(conn.getRemoteSocketAddress().getHostString() + " has left the room!");
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        try {
            String[] result = message.split(";");
            if (result.length < 1) {
                return;
            }
            switch (result[0]) {
                case "setCursorColumn":
                    if (result.length == 2) {
                        ld.setCursorColumn(Integer.parseInt(result[1]));
                    }
                    break;
                case "setCursorRow":
                    if (result.length == 2) {
                        ld.setCursorRow(Integer.parseInt(result[1]));
                    }
                    break;
                case "setBlinkRate":
                    if (result.length == 2) {
                        ld.setBlinkRate(Integer.parseInt(result[1]));
                    }
                    break;
                case "clearText":
                    if (result.length == 1) {
                        ld.clearText();
                    }
                    break;
                case "displayText":
                    if (result.length == 3) {
                        ld.displayText(result[2], Integer.parseInt(result[1]));
                    }
                    break;
                case "displayTextAt":
                    if (result.length == 5) {
                        ld.displayTextAt(Integer.parseInt(result[1]),
                                Integer.parseInt(result[2]), result[4], Integer.parseInt(result[3]));
                    }
                    break;
                default:
                    break;
            }
            System.out.println(conn.getRemoteSocketAddress() + ": " + message);
        } catch (JposException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void onFragment(WebSocket conn, Framedata fragment) {
        System.out.println("received fragment: " + fragment);
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        WebSocketImpl.DEBUG = false;
        int port = 1313; // 843 flash policy port
        try {
            port = Integer.parseInt(args[ 0]);
        } catch (Exception ex) {
        }
        posiflexServer s = new posiflexServer(port);
        s.start();
        System.out.println("posiflexServer started on port: " + s.getPort());
        BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String in = sysin.readLine();
            s.sendToAll(in);
            if (in.equals("exit")) {
                s.stop();
                break;
            } else if (in.equals("restart")) {
                s.stop();
                s.start();
                break;
            }
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        ex.printStackTrace();
        if (conn != null) {
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

    /**
     * Sends <var>text</var> to all currently connected WebSocket clients.
     *
     * @param text The String to send across the network.
     * @throws InterruptedException When socket related I/O errors occur.
     */
    public void sendToAll(String text) {
        Collection<WebSocket> con = connections();
        synchronized (con) {
            for (WebSocket c : con) {
                c.send(text);
            }
        }
    }
}
